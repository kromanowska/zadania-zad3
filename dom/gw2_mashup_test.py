# -*- encoding: utf-8 -*-

import unittest
from gw2_mashup_json import *
from gw2_mashup_dict import *
from gw2_mashup_web import *


class Gw2TestCase(unittest.TestCase):
    def test_check_api(self):
        address = 'https://api.guildwars2.com/v1/map_names.json'
        json = check_api_server(address)
        self.assertIsNotNone(json)
        self.assertGreater(len(json), 0)

    def test_check_api_error1(self):
        address = 'https://api.gdsdsdames.com'
        params = {'aaa': 'bbb'}
        self.assertRaises(SystemExit, lambda: check_api_server(address, params=params))

    def test_check_api_error2(self):
        address = ''
        params = {'': ''}
        self.assertRaises(SystemExit, lambda: check_api_server(address, params=params))

    def test_event_details(self):
        event_details = get_event_details_json('D56DD717-182B-4D11-9B20-B761C2A572A3')
        self.assertIsNotNone(event_details)
        self.assertGreater(len(event_details), 0)
        self.assertIsNotNone(event_details['events']['D56DD717-182B-4D11-9B20-B761C2A572A3']['level'])
        self.assertIsNotNone(event_details['events']['D56DD717-182B-4D11-9B20-B761C2A572A3']['name'])

    def test_event(self):
        event = get_event_json(2008, 943)
        self.assertIsNotNone(event)
        self.assertGreater(len(event), 0)
        self.assertIsNotNone(event['events'][0]['event_id'])
        self.assertIsNotNone(event['events'][0]['state'])

    def test_event_dict1(self):
        event = get_event_json(2008, 943)
        dict1 = event_status_id(event)
        for e in dict1:
            self.assertIsNotNone(dict1[e]['status'])

    def test_event_dict2(self):
        event = get_event_json(2008, 943)
        dict1 = complete_events(event_status_id(event))
        for e in dict1:
            self.assertIsNotNone(e[1]['name'])
            self.assertIsNotNone(e[1]['level'])
            self.assertIsNotNone(e[1]['status'])

    def test_color(self):
        self.assertEqual(return_event_color('Inactive'), 'grey')
        self.assertEqual(return_event_color('Active'), 'green')
        self.assertEqual(return_event_color('Success'), 'gold')
        self.assertEqual(return_event_color('Fail'), 'red')
        self.assertEqual(return_event_color('Warmup'), 'saddlebrown')
        self.assertEqual(return_event_color('Preparation'), 'deepskyblue')

    def test_html(self):
        event = get_event_json(2008, 943)
        dict1 = complete_events(event_status_id(event))
        create_site('Vabbi', 'Tower of Madness', dict1)
        template = open('template.html').read()
        page = open('status_page.html').read()
        self.assertFalse('%s' in page)
        self.assertTrue('%s' in template)


if __name__ == '__main__':
    unittest.main()
