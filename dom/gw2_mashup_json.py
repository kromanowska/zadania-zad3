# -*- encoding: utf-8 -*-

import requests
import sys


def get_maps_json():
    print u'LISTA MAP:'
    map_names_address = 'https://api.guildwars2.com/v1/map_names.json'
    map_names_json = check_api_server(map_names_address)

    for map in map_names_json:
        print '\t', map['name']

    choosen_map_name = raw_input(u'\r\nWybierz jeden z powyższych:')
    return choosen_map_name, map_names_json


def get_world_json():
    print u'LISTA SERWERÓW:'
    world_names_address = 'https://api.guildwars2.com/v1/world_names.json'
    world_names_json = check_api_server(world_names_address)

    for world in world_names_json:
        print '\t', world['name']

    choosen_world_name = raw_input(u'\r\nWybierz jeden z powyższych:')
    return choosen_world_name, world_names_json


def get_event_json(world_id, map_id):
    event_id_address = 'https://api.guildwars2.com/v1/events.json'
    event_id_params = {'world_id': world_id, 'map_id': map_id}
    return check_api_server(event_id_address, event_id_params)


def get_event_details_json(event_id):
    event_details_address = 'https://api.guildwars2.com/v1/event_details.json'
    event_details_params = {'event_id': event_id}
    return check_api_server(event_details_address, event_details_params)


def check_api_server(address, params=None):
    try:
        request = requests.get(address, params=params, timeout=10.0)
        json_data = request.json()
        return json_data
    except Exception:
        print u'\r\nSerwer API jest w tej chwili niedostępny.\r\nProszę spróbować później.'
        sys.exit()