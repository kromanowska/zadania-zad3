# -*- encoding: utf-8 -*-

import operator
from gw2_mashup_json import get_event_details_json


def event_status_id(event_json):
    events = {}
    for event in event_json['events']:
        events[event['event_id']] = {'status': event['state']}
    return events


def complete_events(events):
    for event in events:
        details = get_event_details_json(event)
        event_details = {
            'name': details['events'][event]['name'],
            'level': details['events'][event]['level']
        }

        events[event].update(event_details)
    sorted_events = sorted(events.iteritems(), key=operator.itemgetter(1))
    return sorted_events