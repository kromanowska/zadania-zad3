# -*- encoding: utf-8 -*-

from gw2_mashup_json import *
from gw2_mashup_web import *
from gw2_mashup_dict import *


def choose(choosed_name, json):
    id = ''
    name = ''
    for item in json:
        if choosed_name and choosed_name in item['name'].lower():
            id = item['id']
            name = item['name']
            break
    if id:
        print u'\r\nWYBRANO : %s\r\n' % name
        return name, id
    else:
        print u'\r\nNie znaleziono wprowadzonej nazwy'
        sys.exit()


if __name__ == '__main__':
    try:
        print u'GUILD WARS 2 - Events status\r\n'

        user_world_name, world_json = get_world_json()
        world_name, world_id = choose(user_world_name, world_json)

        user_map_name, map_json = get_maps_json()
        map_name, map_id = choose(user_map_name, map_json)

        print u'Trwa pobieranie informacji o zdarzeniach. Może to potrwać kilka minut...'
        events = event_status_id(get_event_json(world_id, map_id))
        sorted_events_with_details = complete_events(events)

        print u'Pobieranie ukończone. Generowanie strony wynikowej...'
        create_site(world_name, map_name, sorted_events_with_details)
        open_web()
    except Exception, ex:
        print u'BŁĄD! Program napotkał nieoczekiwany problem.\r\n'
