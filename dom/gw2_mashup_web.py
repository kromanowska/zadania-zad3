# -*- encoding: utf-8 -*-

import sys
import os
import webbrowser


def return_event_color(event_status):
    if event_status == 'Inactive':
        return 'grey'
    elif event_status == 'Active':
        return 'green'
    elif event_status == 'Success':
        return 'gold'
    elif event_status == 'Fail':
        return 'red'
    elif event_status == 'Warmup':
        return 'saddlebrown'
    elif event_status == 'Preparation':
        return 'deepskyblue'


def fill_html_events(sorted_events):
    table = '''
    <table style="width:900px">
        <tr>
            <td style="width:600px">%s</td>
            <td style="width:100px">%s</td>
            <td style="width:200px; color: %s"><b>%s</b></td>
        </tr>
    </table>
    <hr/>
    '''
    events_html = ''
    for event in sorted_events:
        events_html += table % (event[1]['name'], event[1]['level'], return_event_color(event[1]['status']), event[1]['status'])
    return events_html


def create_site(world, map, sorted_events):
    template = open('template.html').read()
    page = open('status_page.html', 'w')
    page.write(template % (world, map, fill_html_events(sorted_events)))
    page.close()


def open_web():
    url = os.path.normcase('file://%s/status_page.html' % os.path.dirname(os.path.realpath(__file__)))
    webbrowser.open(url)
    sys.exit()